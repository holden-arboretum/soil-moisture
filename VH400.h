/*****************************************************************************
**  File name: VH400.h
**  Description: Driver for the VH400 soil moisture sensor
**  Notes:
**  Author(s): Humphrey, Dylan; Lapetina, Elyh
**  Created: 4/24/2019
**  Last Modified: 4/24/2019
**  Changes:
******************************************************************************/

#ifndef VH400_H
#define VH400_H

#include "arduino.h"

class VH400 {
public:
  VH400(int pin);
  float read();
  float read_raw();
private:
  int analog_in;
};

#endif
