#include "VH400.h"

VH400::VH400(int pin) {
  analog_in = pin;
}

float VH400::read() {
  float raw = read_raw();
  if (raw <= 1.1) {
    return 10 * raw - 1;
  } else if (raw <= 1.3) {
    return 25 * raw - 17.5;
  } else if (raw <= 1.82) {
    return 48.08 * raw - 47.5;
  } else if (raw <= 2.2) {
    return 26.32 * raw - 7.89;
  } else {
    return 62.5 * raw - 87.5;
  }
}

float VH400::read_raw() {
  int value = analogRead(analog_in);
  return (3.3/1024) * value;
}
