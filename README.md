# VH400 Soil Mositure Sensor

A very lightweight library written for the Vegetronix VH400 soil moisture sensor

## Example

```
const int analogIn = 5;

// pass in our analog pin
VH400 soil(analogIn);

void setup() {
  // do things
}

void loop() {
  // get a raw voltage reading
  float raw = soil.read_raw();
  
  // get a value in volumetric water content
  float vwc = soil.read();

  Serial.print("Raw: ");
  Serial.println(raw);
  Serial.print("VWC: ");
  Serial.println(vwc);
}

```

## Functions
`VH400(int pin)`

Constructor for the function. `pin` is the analog input pin which the soil moisture sensor is connected to

---

`float read_raw()`

Instruct the microcontroller to perform an analog read on the pin specified in the constructor. This function
converts the analog read into a voltage. 

Note: Currently it is assumed that a 3.3V reference and a 10bit ADC is used.

---
`float read()`

Will perform the analog read and convert the voltage value into a volumetric water content as described by
the datasheet of the VH400 sensor.

